CUDA Simple Intro
=================

Code from [this](https://devblogs.nvidia.com/parallelforall/even-easier-introduction-cuda/)
Nvidia article on baby's first CUDA program.

The included makefile assumes the user is using Clang++ and has CUDA installed
properly as indicated in the article.
